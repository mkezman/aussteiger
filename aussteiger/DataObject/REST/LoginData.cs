﻿namespace aussteiger.DataObject.REST
{
    public class LoginData
    {
        public string Mail { get; set; }
        public string Password { get; set; }
    }
}
