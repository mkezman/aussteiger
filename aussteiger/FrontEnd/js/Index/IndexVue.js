﻿function InitVue() {
    var app = new Vue({
        el: '#MainContent',
        data: indexViewState,
        methods: {
            vueOpenStory: function (data) { openStory(data); }
        }
    });

    getThreeStories();
}