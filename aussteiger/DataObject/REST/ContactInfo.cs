﻿namespace aussteiger.DataObject.REST
{
    public class ContactInfo
    {
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Message { get; set; }
    }
}
