﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aussteiger.DataObject.REST
{
    class CountryQuery
    {
        public int PopulationMin { get; set; }
        public int PopulationMax { get; set; }
        public string Continent { get; set; }
    }
}
