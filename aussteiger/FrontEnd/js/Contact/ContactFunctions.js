function sendMessage() {
    if (contactViewState.name === "") {
        alert("Sie müssen einen Namen angeben.");
        return;
    }
    if (contactViewState.mail === "") {
        alert("Sie müssen eine E-Mail angeben.");
        return;
    }
    if (contactViewState.message === "") {
        alert("Sie müssen eine Nachricht angeben.");
        return;
    }
    $.ajax({ url: "/Contact", type: "PUT", data: JSON.stringify(contactViewState) }).done(function (data) {
        if (data === "") {
            alert("Nachricht erfolgreich übermittelt.");
            contactViewState.name = "";
            contactViewState.mail = "";
            contactViewState.message = "";
            return;
        }
        alert("Sendung fehlgeschlagen");
    }).fail(function () {
        alert("Sendung fehlgeschlagen");
    });
}
//# sourceMappingURL=ContactFunctions.js.map