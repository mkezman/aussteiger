﻿function showResult(show: boolean) {
    dropoutViewState.showResult = show;
}

function getCountries() {
    if (dropoutViewState.countryQuery.Continent === "" || dropoutViewState.countryQuery.PopulationMin === "" || dropoutViewState.countryQuery.PopulationMax === "") {
        alert("Sie müssen alle Felder ausfüllen.");
        return;
    }

    $.post("/Countries",
        JSON.stringify(dropoutViewState.countryQuery),
        data => {
            if (data === "NoMatch") {
                dropoutViewState.noResult = true;
            } else {
                dropoutViewState.noResult = false;
                dropoutViewState.result = JSON.parse(data);
            }
            showResult(true);
        }).fail(() => {
        alert(AlertNoData);
    });
}