﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using aussteiger.Attributes;
using aussteiger.Components.Persistence;
using aussteiger.Components.Persistence.ORM;
using aussteiger.DataObject;
using aussteiger.DataObject.REST;
using aussteiger.Utils;


// ReSharper disable UnusedMember.Global
namespace aussteiger.Components.Communication
{
    public class RestInterface
    {
        private readonly HttpListenerContext _ctx;
        private readonly PersistenceManager _persistenceManager;
        private readonly SessionManager _sessionManager;
        
        public RestInterface(HttpListenerContext ctx, SessionManager sessionManager, PersistenceManager persistenceManager)
        {
            _ctx = ctx;
            _persistenceManager = persistenceManager;
            _sessionManager = sessionManager;
        }


        #region Storie

        [UrlMapping("ThreeStories", HttpMethods.GET)]
        public void GetThreeStories()
        {
            Console.WriteLine($"CM | {DateTime.Now} | Get three stories");
            var stories = _persistenceManager.GetThreeStories();
            ResponseWriter.WriteText(_ctx.Response, SerializationUtility.SerializeObject(stories));
        }

        [UrlMapping("ThreeStoriesWithoutActive", HttpMethods.POST)]
        public void GetThreeStoriesWithoutActive(string rqBody)
        {
            Console.WriteLine($"CM | {DateTime.Now} | Get three without id");
            var stories = _persistenceManager.GetThreeStories(rqBody);
            ResponseWriter.WriteText(_ctx.Response, SerializationUtility.SerializeObject(stories));
        }

        [UrlMapping("Story", HttpMethods.POST)]
        public void GetStoryById(string rqBody)
        {
            Console.WriteLine($"CM | {DateTime.Now} | Get story by id");
            var story = _persistenceManager.GetStoryById(rqBody);
            ResponseWriter.WriteText(_ctx.Response, SerializationUtility.SerializeObject(story));
        }

        #endregion

        #region Contact

        [UrlMapping("Contact", HttpMethods.PUT)]
        public string SaveContact(string rqBody)
        {
            Console.WriteLine($"CM | {DateTime.Now} | Save contact");
            var contactInfo = SerializationUtility.DeserializeObject<ContactInfo>(rqBody);

            try
            {
                using (var ctx = MySqlHelper.GetDataContext())
                {

                    var contact = new Contacts()
                    {
                        Name = contactInfo.Name,
                        Mail = contactInfo.Mail,
                        Message = contactInfo.Message
                    };

                    ctx.Contacts.InsertOnSubmit(contact);
                    ctx.SubmitChanges();
                    return "";
                }
            }
            catch (Exception e)
            {
                return "failed";
            }
        }

        #endregion

        #region Country

        [UrlMapping("Countries", HttpMethods.POST)]
        public void GetCountries(string rqBody)
        {
            Console.WriteLine($"CM | {DateTime.Now} | Get countries");
            var countries = _persistenceManager.GetCountries(rqBody);
            ResponseWriter.WriteText(_ctx.Response,
                countries.Any()
                    ? SerializationUtility.SerializeObject(countries)
                    : SerializationUtility.SerializeObject("NoMatch"));
        }

        #endregion
    }
}
