﻿// ReSharper disable InconsistentNaming
class Story {
    ID: number;
    Header: string;
    Name: string;
    Text: string;
    Date: string;
    IMg: number;
}

class CountryQuery {
    PopulationMin: number;
    PopulationMax: number;
    Continent: string;
}