﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using aussteiger.Attributes;
using aussteiger.Components.Communication;
using aussteiger.Components.Persistence;
using aussteiger.Components.Persistence.ORM;
using aussteiger.DataObject;
using aussteiger.Utils;

namespace aussteiger.Components.Communication
{
    public class CommunicationManager
    {
        private readonly SessionManager _sessionManager;
        private readonly PersistenceManager _persistenceManager;
        private Dictionary<string, Dictionary<string, MethodInfo>> _restInterfaceMethods;
        private bool _stopServer;
        private readonly HttpListener _httpListener = new HttpListener();

        public CommunicationManager(PersistenceManager persistenceManager)
        {
            _persistenceManager = persistenceManager;
            _sessionManager = new SessionManager();

            RegisterInterfaceMethods();
        }


        private void RegisterInterfaceMethods()
        {
            _restInterfaceMethods = new Dictionary<string, Dictionary<string, MethodInfo>>();

            foreach (var methodInfo in typeof(RestInterface).GetMethods())
            {
                foreach (var urlMapping in methodInfo.GetCustomAttributes(true).OfType<UrlMapping>())
                {
                    if (!_restInterfaceMethods.ContainsKey(urlMapping.Map))
                    {
                        _restInterfaceMethods.Add(urlMapping.Map, new Dictionary<string, MethodInfo>());
                    }

                    _restInterfaceMethods[urlMapping.Map].Add(urlMapping.HttpMethod, methodInfo);
                }
            }

            Console.WriteLine("Available services:");

            foreach (var restInterfaceMethod in _restInterfaceMethods)
            {
                foreach (var methodInfo in restInterfaceMethod.Value)
                {
                    Console.WriteLine($"- {restInterfaceMethod.Key} - {methodInfo.Key}");
                }
            }
        }

        public void Start()
        {
            var serverAdress = $"http://*:{Global.Settings.Port}/";
            _httpListener.Prefixes.Add(serverAdress);
            _httpListener.Start();

            Console.WriteLine($"CM | {DateTime.Now} | Start");

            while (!_stopServer)
            {
                try
                {
                    GetAndHandleRequest();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"CM | {DateTime.Now} | {e.Message}");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }

            Stop();
        }

        public void Stop()
        {
            if (!_httpListener.IsListening) return;
            Console.WriteLine($"CM | {DateTime.Now} | Stop");
            _httpListener.Stop();
            _httpListener.Close();
        }

        private void GetAndHandleRequest()
        {
            var ctx = _httpListener.GetContext();

            if (GetMethodName(ctx) == "stop")
            {
                ctx.Response.StatusCode = 200;
                ctx.Response.Close();
                _stopServer = true;
                return;
            }

            ThreadPool.QueueUserWorkItem(
                _ =>
                {
                    var httpMethod = ctx.Request.HttpMethod;

                    switch (httpMethod)
                    {
                        case HttpMethods.GET:
                            HandleGetOrDeleteRequest(ctx, httpMethod);
                            break;
                        case HttpMethods.POST:
                            HandlePostOrPutRequest(ctx, httpMethod);
                            break;
                        case HttpMethods.DELETE:
                            HandleGetOrDeleteRequest(ctx, httpMethod);
                            break;
                        case HttpMethods.PUT:
                            HandlePostOrPutRequest(ctx, httpMethod);
                            break;
                        default:
                            ctx.Response.StatusCode = 400;
                            break;
                    }

                    ctx.Response.Close();
                });
        }

        private void HandlePostOrPutRequest(HttpListenerContext ctx, string httpMethod)
        {
            var methodName = GetMethodName(ctx);
            Console.WriteLine($"{httpMethod} => {methodName} :: {ctx.Request.Url}");
            var method = GetMethodSpecifiedInUrl(methodName, httpMethod);

            if (method == null)
            {
                ctx.Response.StatusCode = 404;
                Console.WriteLine($"Unknown method | {methodName}");
                ResponseWriter.WriteText(ctx.Response, "Error - method does not exists or is not supported");
                return;
            }

            try
            {
                var requestBody = GetRequestBody(ctx.Request);
                method.Invoke(new RestInterface(ctx, _sessionManager, _persistenceManager), requestBody); //ToDo RestInterface
            }
            catch (TargetInvocationException e)
            {
                HandleTargetInvocationException(ctx, e);
            }
            catch (Exception e)
            {
                HandleException(ctx, e);
            }
        }

        private void HandleGetOrDeleteRequest(HttpListenerContext ctx, string httpMethod)
        {
            var methodName = GetMethodName(ctx);
            var strParams = GetParameterFromUrl(ctx);
            Console.WriteLine($"{httpMethod} => {methodName} :: [{string.Join(",", strParams)}]");
            var method = GetMethodSpecifiedInUrl(methodName, httpMethod);

            if (method == null)
            {
                LoadExistingPageOrWriteError(ctx, methodName, strParams);
                return;
            }

            try
            {
                var @params = GetParametersWithCorrectType(method, strParams);
                method.Invoke(new RestInterface(ctx, _sessionManager, _persistenceManager), @params);  //Todo RestInterface
            }
            catch (TargetInvocationException e)
            {
                HandleTargetInvocationException(ctx, e);
            }
            catch (Exception e)
            {
                HandleException(ctx, e);
            }
        }

        private static void HandleException(HttpListenerContext ctx, Exception e)
        {
            Console.WriteLine($"CM | {DateTime.Now} | {e.Message}");
            var errorMessage = $"The call {ctx.Request.Url} caused an exception";
            Console.WriteLine(errorMessage);
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            try
            {
                ctx.Response.StatusCode = 400;
                ResponseWriter.WriteText(ctx.Response, errorMessage);
            }
            catch
            {
                //throw another exeption if the client connection got lost
            }
        }

        private static void HandleTargetInvocationException(HttpListenerContext ctx, Exception e)
        {
            Console.WriteLine($"CM | {DateTime.Now} | {e.InnerException?.Message ?? e.Message}");
            var errorMessage = $"The call {ctx.Request.Url} caused an exception";
            Console.WriteLine(errorMessage);
            Console.WriteLine(e.InnerException?.Message ?? e.Message);
            Console.WriteLine(e.InnerException?.StackTrace ?? e.StackTrace);
            try
            {
                ctx.Response.StatusCode = 400;
                ResponseWriter.WriteText(ctx.Response, errorMessage);
            }
            catch
            {
                //throw another exeption if the client connection got lost
            }
        }

        private void LoadExistingPageOrWriteError(HttpListenerContext ctx, string firstUrlSegment, string[] furtherSegments)
        {
            var url = firstUrlSegment;
            if (furtherSegments != null && furtherSegments.Length > 0)
            {
                url += "/" + string.Join("/", furtherSegments);
            }

            if (string.IsNullOrEmpty(firstUrlSegment) || firstUrlSegment == "/")
            {
                ctx.Response.Redirect("/login.html");
                return;
            }

            if (PageLoader.Instance.ContainsPage(url))
            {
                var isNotValidSession = _sessionManager.GetSessionInfo(ctx.Request.Cookies[Global.CookieName]?.Value) == null;
                //Todo Remove next line before go live - line disables login
                isNotValidSession = false;
                if (url != "login.html" && isNotValidSession && url.EndsWith(".html"))
                {
                    ctx.Response.Redirect("/login.html");
                    return;
                }

                ResponseWriter.WritePage(ctx.Response, url, PageLoader.Instance.GetPage(url));
                return;
            }

            ctx.Response.StatusCode = 404;
            var errorMessage = $"The Page {url} does not exist";
            Console.WriteLine(errorMessage);
            ResponseWriter.WriteText(ctx.Response, errorMessage);
        }

        private static string GetMethodName(HttpListenerContext ctx)
        {
            return ctx.Request.Url.Segments.Length < 2 ? string.Empty : ctx.Request.Url.Segments[1].Replace("/", "");
        }

        private MethodInfo GetMethodSpecifiedInUrl(string methodName, string httpMethod)
        {
            if (!_restInterfaceMethods.ContainsKey(methodName)) return null;
            return !_restInterfaceMethods[methodName].ContainsKey(httpMethod) ? null : _restInterfaceMethods[methodName][httpMethod];
        }

        private object[] GetRequestBody(HttpListenerRequest request)
        {
            using (var inputStream = request.InputStream)
            {
                using (var reader = new StreamReader(inputStream, Encoding.UTF8))
                {
                    return new object[] { reader.ReadToEnd() };
                }
            }
        }

        private static object[] GetParametersWithCorrectType(MethodInfo method, string[] strParams)
        {
            return method.GetParameters().Select((p, i) => Convert.ChangeType(strParams[i], p.ParameterType)).ToArray();
        }

        private static string[] GetParameterFromUrl(HttpListenerContext ctx)
        {
            return ctx.Request.Url
                .Segments
                .Skip(2)
                .Select(s => s.Replace("/", ""))
                .Select(Uri.UnescapeDataString)
                .ToArray();
        }
    }
}
