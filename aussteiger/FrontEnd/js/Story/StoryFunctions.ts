﻿// ReSharper disable once InconsistentNaming
const AlertNoData = "Es konnten nicht alle Daten geladen werden";

function getThreeStories() {
    $.get("/ThreeStories",
        data => {
            indexViewState.stories = JSON.parse(data);
            for (var i = 0; i < indexViewState.stories.length; i++) {
                indexViewState.stories[i].Date = convertDateStringToDate(indexViewState.stories[i].Date);
            }
            return;
        }).fail(() => {
        alert(AlertNoData);
    });
}

function getThreeStoriesWithoutActive(id: string) {
    $.post("/ThreeStoriesWithoutActive", 
        id,
        data => {
            storyViewState.stories = JSON.parse(data);
            for (var i = 0; i < storyViewState.stories.length; i++) {
                storyViewState.stories[i].Date = convertDateStringToDate(storyViewState.stories[i].Date);
            }
            return;
        }).fail(() => {
        alert(AlertNoData);
    });
}

function getActiveStory() {
    var urlParams = new URLSearchParams(window.location.search);
    var id = urlParams.get("id");
    if (id === "") id = "1";

    $.post("/Countries",
        id,
        data => {
            storyViewState.activeStory = JSON.parse(data);
            storyViewState.activeStory.Date = convertDateStringToDate(storyViewState.activeStory.Date);
            getThreeStoriesWithoutActive(id);
        }).fail(() => {
        alert(AlertNoData);
    });
}

function openStory(storyId: number) {
    window.open(`./story.html?id=${storyId}`, "_self");
}

function convertDateStringToDate(dateString: string) {
    return dateString.substring(8, 10) + "." + dateString.substring(5, 7) + "." + dateString.substring(0, 4);
}