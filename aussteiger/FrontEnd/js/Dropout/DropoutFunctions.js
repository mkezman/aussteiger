function showResult(show) {
    dropoutViewState.showResult = show;
}
function getCountries() {
    if (dropoutViewState.countryQuery.Continent === "" || dropoutViewState.countryQuery.PopulationMin === "" || dropoutViewState.countryQuery.PopulationMax === "") {
        alert("Sie müssen alle Felder ausfüllen.");
        return;
    }
    $.post("/Countries", JSON.stringify(dropoutViewState.countryQuery), function (data) {
        if (data === "NoMatch") {
            dropoutViewState.noResult = true;
        }
        else {
            dropoutViewState.noResult = false;
            dropoutViewState.result = JSON.parse(data);
        }
        showResult(true);
    }).fail(function () {
        alert(AlertNoData);
    });
}
//# sourceMappingURL=DropoutFunctions.js.map