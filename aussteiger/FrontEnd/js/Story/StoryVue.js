﻿function InitVue() {
    var app = new Vue({
        el: '#MainContent',
        data: storyViewState,
        methods: {
            vueOpenStory: function (data) { openStory(data); }
        }
    });

    getActiveStory();
}