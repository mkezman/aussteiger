﻿using System;

namespace aussteiger.DataObject
{
    public class Settings
    {
        public string Port { get; set; }
        public string Server { get; set; }
        public string Database { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            var nl = Environment.NewLine;
            return $"Port: {Port}" + nl +
                   $"Server: {Server}" + nl +
                   $"Database: {Database}" + nl +
                   $"UserID: {UserID}" + nl +
                   $"Password: {Password}";
        }
    }
}
