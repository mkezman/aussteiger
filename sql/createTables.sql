DROP TABLE IF EXISTS einwohnerdichte;
CREATE TABLE `einwohnerdichte` (
    `id` INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` VARCHAR(250),
    `einwohner` INTEGER(25),
    `einwohnerkm` INTEGER(10),
    `kontinent` VARCHAR (25)
);

INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Burundi", "6502000", "234", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Komoren", "727000", "325", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Dschibuti", "644000", "28", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Eritrea", "3816000", "32", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Äthiopien", "64459000", "58", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kenia", "31293000", "54", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Madagaskar", "16437000", "28", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Malawi", "11572000", "98", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mauritius", "1171000", "574", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mosambik", "18644000", "23", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Réunion(franz)", "732000", "292", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Ruanda", "7949000", "302", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Seychellen", "81000", "179", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Somalia", "9157000", "14", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Uganda", "24023000", "102", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tansania", "35965000", "38", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Sambia", "10649000", "14", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Simbabwe", "12852000", "33", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Angola", "13527000", "11", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kamerun", "15203000", "32", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("ZentralafrikanischeRepublik", "3782000", "6", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tschad", "8135000", "6", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kongo", "3110000", "9", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Dem Rep Kongo", "52522000", "22", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Äquatorialguinea", "470000", "17", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Gabun", "1262000", "5", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("São Tomé and Príncipe", "140000", "145", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Algerien", "30841000", "13", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Ägypten", "69080000", "69", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Libyen", "5408000", "3", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Marokko", "30430000", "68", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Sudan", "31809000", "13", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tunesien", "9562000", "58", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("(West-)Sahara", "260000", "1", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Botsuana", "1554000", "3", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Lesotho", "2057000", "68", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Namibia", "1788000", "2", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Südafrika", "43792000", "36", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Swasiland", "938000", "54", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Burkina Faso", "11856000", "43", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kap Verde", "437000", "108", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Elfenbeinküste", "16349000", "51", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Gambia", "1337000", "118", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Ghana", "19734000", "83", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Guinea", "8274000", "34", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Guinea-Bissau", "1227000", "34", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Liberia", "3108000", "28", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mali", "11677000", "9", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mauretanien", "2747000", "3", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Niger", "11227000", "9", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Nigeria", "116929000", "127", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("St Helena(brit)", "6000", "52", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Senegal", "9662000", "49", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Sierra Leone", "4587000", "64", "Afrika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Togo", "4657000", "82", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("China", "1284972000", "134", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Hongkong(Sonderverwaltungszone)", "6961000", "6661", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Macau(Sonderverwaltungszone)", "449000", "24924", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Nord-Korea", "22428000", "186", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Japan", "127335000", "337", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mongolei", "2559000", "2", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Süd-Korea", "47069000", "475", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Afghanistan", "22474000", "34", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bangladesch", "140369000", "975", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bhutan", "2141000", "46", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Indien", "1025096000", "312", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Iran", "71369000", "43", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kasachstan", "16095000", "6", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kirgistan", "4986000", "25", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Malediven", "300000", "1006", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Nepal", "23593000", "168", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Pakistan", "144971000", "182", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Sri Lanka", "19104000", "291", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tadschikistan", "6135000", "43", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Turkmenistan", "4835000", "10", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Usbekistan", "25257000", "56", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Brunei", "335000", "58", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kambodscha", "13441000", "74", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Osttimor", "750000", "50", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Indonesien", "214840000", "113", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Laos", "5403000", "23", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Malaysia", "22633000", "69", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Myanmar (Burma, Birma)", "48364000", "71", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Philippinen", "77131000", "257", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Singapur", "4108000", "6647", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Thailand", "63584000", "124", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Vietnam", "79175000", "239", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Aserbaidschan", "8096000", "93", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bahrain", "652000", "961", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Zypern", "790000", "85", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Georgien", "5239000", "75", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Irak", "23584000", "54", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Israel", "6172000", "293", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Jordanien", "5051000", "52", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kuwait", "1971000", "111", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Libanon", "3556000", "342", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("BesetztePalästinensischeGebiete", "3311000", "537", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Oman", "2622000", "12", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Katar", "575000", "52", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Saudi-Arabien", "21028000", "10", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Syrien", "16610000", "90", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Türkei", "67632000", "87", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("VereinigteArabischeEmirate", "2654000", "32", "Asien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Jemen", "19114000", "36", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Weißrussland", "10147000", "49", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bulgarien", "7867000", "71", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tschechien", "10260000", "130", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Ungarn", "9917000", "107", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Polen", "38577000", "119", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Moldau (Moldawien)", "4285000", "127", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Rumänien", "22388000", "94", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Russische Föderation, Russland", "144664000", "8", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Slowakei", "5403000", "110", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Ukraine", "49112000", "81", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kanalinseln(brit)", "145000", "741", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Dänemark", "5333000", "124", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Estland", "1377000", "31", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Färöer(dän)", "47000", "33", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Finnland", "5178000", "15", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Island", "281000", "3", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Irland", "3841000", "55", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Insel Man(brit)", "76000", "129", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Lettland", "2406000", "37", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Litauen", "3689000", "57", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Norwegen", "4488000", "14", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Schweden", "8833000", "20", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("GroßbritannienundNordirland", "59542000", "244", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Albanien", "3145000", "109", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Andorra", "90000", "198", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bosnien-Herzegowina", "4067000", "80", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kroatien", "4655000", "82", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Gibraltar(brit)", "27000", "4428", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Griechenland", "10623000", "80", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Vatikanstadt (Heiliger Stuhl)", "1000", "1770", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Italien", "57503000", "191", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Malta", "392000", "1240", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Portugal", "10033000", "109", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("San Marino", "27000", "440", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Slowenien", "1985000", "98", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Spanien", "39921000", "79", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mazedonien", "2044000", "79", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Jugoslawien", "10538000", "103", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Österreich", "8075000", "96", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Belgien", "10264000", "336", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Frankreich", "59453000", "108", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Deutschland", "82007000", "230", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Liechtenstein", "33000", "206", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Luxemburg", "442000", "171", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Monaco", "34000", "22641", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Niederlande", "15930000", "390", "Europa");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Schweiz", "7170000", "174", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Anguilla(brit)", "12000", "122", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Antigua und Barbuda", "65000", "148", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Aruba(niederl)", "104000", "539", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bahamas", "308000", "22", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Barbados", "268000", "624", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("BritischeJungferninseln(brit)", "24000", "159", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kaimaninseln(brit)", "40000", "153", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kuba", "11237000", "101", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Dominica", "71000", "94", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Dominikanische Republik", "8507000", "175", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Grenada", "94000", "273", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Guadeloupe(franz)", "431000", "253", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Haiti", "8270000", "298", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Jamaika", "2598000", "236", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Martinique(franz)", "386000", "350", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Montserrat(brit)", "3000", "33", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("NiederländischeAntillen(niederl)", "217000", "271", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Puerto Rico(amerik)", "3952000", "444", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("StKittsundNevis", "38000", "146", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("St Lucia", "149000", "240", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("StVincentunddieGrenadinen", "114000", "294", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Trinidad und Tobago", "1300000", "253", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Turks-andCaicosinseln(brit)", "17000", "40", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("AmerikanischeJungferninseln(amerik)", "122000", "352", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Belize", "231000", "10", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Costa Rica", "4112000", "80", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("El Salvador", "6400000", "304", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Guatemala", "11687000", "107", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Honduras", "6575000", "59", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mexiko", "100368000", "51", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Nicaragua", "5208000", "40", "Mittelamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Panama", "2899000", "38", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Argentinien", "37488000", "14", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bolivien", "8516000", "8", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Brasilien", "172559000", "20", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Chile", "15402000", "20", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kolumbien", "42803000", "38", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Ecuador", "12880000", "45", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Falklandinseln(brit)", "2000", "0", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Französisch-Guyana(franz)", "170000", "2", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Guyana", "763000", "4", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Paraguay", "5636000", "14", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Peru", "26093000", "20", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Suriname", "419000", "3", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Uruguay", "3361000", "19", "Südamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Venezuela", "24632000", "27", "Nordamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Bermuda(brit)", "63000", "1196", "Nordamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kanada", "31015000", "3", "Nordamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Grönland(dän)", "56000", "0", "Nordamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("St Pierre und Miquelon(franz)", "7000", "29", "Nordamerika");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("VereinigteStaatenvonAmerika,USA", "285926000", "31", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Australien", "19338000", "3", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Neuseeland", "3808000", "14", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Fidschi", "823000", "45", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Neukaledonien(franz)", "220000", "12", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Papua-Neuguinea", "4920000", "11", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Salomonen", "463000", "16", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Vanuatu", "202000", "17", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Guam(amerik)", "158000", "292", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Kiribati", "84000", "116", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Marshallinseln", "52000", "286", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Mikronesien", "126000", "179", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Nauru", "13000", "596", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("NördlicheMarianen(amerik)", "76000", "164", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Palau", "20000", "43", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Amerikanisch-Samoa(amerik)", "70000", "351", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Cookinseln(neuseel)", "20000", "84", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Französisch-Polynesien(franz)", "237000", "59", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Niue(neuseel)", "2000", "8", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Pitcairninseln(brit)", "0", "14", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Samoa", "159000", "56", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tokelau(neuseel)", "1000", "121", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tonga", "99000", "133", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("Tuvalu", "10000", "397", "Australien");
INSERT INTO `einwohnerdichte` (`name`, `einwohner`, `einwohnerkm`, `kontinent`) VALUES ("WallisundFutuna(franz)", "15000", "73", "");


DROP TABLE IF EXISTS `contacts`;
CREATE TABLE contacts (
    `id` INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` VARCHAR(250),
    `mail` VARCHAR(250),
    `message` VARCHAR(1000));

DROP TABLE IF EXISTS `sectsgermany`;
CREATE TABLE sectsgermany (
    `id` INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `description` VARCHAR(150),
    `membercount` INTEGER(11));

INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Neuapostolische Kirche", 445000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Zeugen Jehovas", 192000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Mormonen", 36000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Vereinigte", 30000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Apostolische Gemeinden", 8000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Neugermanen", 25000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Christengemeinschaft", 20000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Bruno-Gröning", 12000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Apostolische Gemeinschaft", 8000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Christliche Wissenschaft", 8000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Universelles Leben", 6000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Bahai", 5000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Apostelamt Juda", 4500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Johannische Kirche", 3500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Gralsbewegung", 2500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Soka Gakkai", 2100);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Boston Church of Christ", 1000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Rajneesh Bewegung", 750);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Fia Lux", 700);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Vereinigungskirche", 500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("ISKON (Krishna)", 390);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Scientology", 5500);

DROP TABLE IF EXISTS `stories`;
CREATE TABLE stories (
    `id` INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `header` VARCHAR(250),
    `name` VARCHAR(150),
    `text` VARCHAR(20000),
    `date` DATETIME,
    `img` VARCHAR(150));

INSERT INTO `stories` (`img`, `date`, `header`, `name`, `text`) VALUES ("images/story-00000000001.jpg", "2018-05-12", "Mein Leben und mein Ausstieg bei den Zeugen Jehovas", "Dietlinde G.",
"Hallo zusammen,<br>
nach langer Überlegung schreibe ich hier doch mal meine Geschichte.<br>
<br>
Ich wurde zu den Zeugen rein geboren. Heißt, sowohl mein Vater als auch meine Mutter waren damals getauft. Als Kind war das alles normal für mich. Ich kannte es ja nicht anders.<br>
<br>
Ich war ein sehr schüchternes Kind, habe mich immer hinter meiner Mutter versteckt und könnte nicht mit fremden Menschen sprechen.<br>
<br>
Im Kindergarten war ich nicht, da es bei uns im Dorf nur einen kirchlichen gab. Meine Leidenszeit fing an als ich in die Schule kam. Alle kannten sich schon aus dem Kindergarten, nur mich kannte niemand. Natürlich wurde auch gleich verkündet dass ich morgens nicht mit den anderen beten durfte (bei uns in der Schule würde noch jeden morgen das Vaterunser gebetet) und das ich keine Geburtstage feiere. Damit hatte ich in unserer kleinen Dorfschule natürlich gleich einen Ruf weg.<br>
<br>
Trotzdem kam ich recht gut durch die Grundschule und fand auch ziemlich viele „weltliche“ Freunde. Meine Eltern waren für Zeugen ziemlich locker und es wurde mir weder vorgeschrieben welche Freunde ich haben muss, noch wurde ich dazu gezwungen in den Dienst zu gehen oder so.<br>
<br>
Trotzdem war die Erwartungshaltung natürlich nicht nur von meinen Eltern, sondern auch von den Ältesten die dass ich ein guter Zeuge werde. Am Anfang sah auch alles danach aus. Ich identifizierte mich mit der Versammlung, ging predigen (Auch wenn ich jedes mal hoffte keinem Klassenkameraden zu begegnen) und wollte mich auch immer mehr in die Versammlung einbringen , mit Aufgaben usw.<br>
<br>
Aber es gab schon immer die andere Seite. Es war mir furchtbar peinlich, dass ich in der Schule nicht gratulieren durfte wenn jemand Geburtstag hatte. Wenn ich wieder an Weihnachten oder St. Martin nicht mit basteln durfte. Wenn die Anderen mit ihren Laternen durch die Straßen zogen. Ich wollte schon immer gern dabei sein. Einmal gratulierte ich meinem Klassenkameraden zum Geburtstag weil ich nicht sagen wollte dass ich das nicht darf. Ich hatte so ein schlechtes Gewissen danach. Ich habe es dann natürlich meiner Mutter erzählt und es gab ein ernstes Gespräch mit meinen Eltern.<br>
<br>
Natürlich wurde ich auch ab und an von meinen Eltern geschlagen, nicht schlimm verprügelt sondern mehr Ohrfeigen, was ich aber trotzdem als sehr schlimm und erniedrigend empfand.<br>
<br>
Ab der weiterführenden Schule habe ich mich stark verändert. Ich wurde selbstbewusster und frecher, fing an langsam zu rebellieren. Das entging natürlich den anderen in der Versammlung nicht und ich wurde auch da zum Außenseiter. Ich war bei den anderen Kindern in der Versammlung ein bisschen das „cool kid“ und wurde einerseits ein bisschen bewundert aber trotzdem wollte natürlich niemand mehr etwas mit mir zu tun haben.<br>
<br>
Meine Mutter ließ sich ausschließen als ich vielleicht 12 war. Ehrlich gesagt weiß ich gar nicht ob sie ausgeschlossen ist, jedenfalls ist sie nicht mehr hin gegangen. Danach mussten wir Kinder uns entscheiden. Eine Zeitlang bin ich mit meinem Vater noch regelmäßig in die Versammlung gegangen. Dann irgendwann nur noch zu Kongressen und zum Gedächtnismahl. Mit glaube ich 15 bin ich dann gar nicht mehr gegangen. Mein Vater fragte zwar immer ob wir mitkommen wollten aber er zwang uns zu nichts.<br>
<br>
Als ich dann mit 18 zuhause ausgezogen bin, habe ich es erstmal richtig krachen lassen. Ich war praktisch nur noch betrunken, bin wahllos mit irgendwelchen Typen ins Bett, habe Drogen genommen, meine Schule abgebrochen, bin mit Punks auf der Straße ein gehangen. Ich war kurz vorm abrutschen und hab dann zum Glück doch noch die Kurve gekriegt. Ich bereue nichts, außer dass ich damals vielen Menschen weh getan habe. Seitdem haben sich meine Tanten und Onkel die noch in der Versammlung waren komplett von mir angewandt. Damit konnte ich leben, ich könnte die sowieso noch nie leiden<br>
<br>
Allgemein bin ich sehr froh, dass meine Eltern doch einigermaßen locker waren und das obwohl mein Vater ein sehr hoher Ältester war. Trotzdem habe ich immer noch große Probleme Nähe zuzulassen und meine Gefühle zu äußern. Es gab in unserer Familie keine Nähe. Es gab keine Umarmungen oder liebe Worte. Als mein Vater letztes Jahr überraschend starb, haben mir viele junge Zeugen gesagt, er wäre wie ein Vater für sie gewesen. Das hat mir sehr weh getan, denn für mich war er es nie. Er hat sich immer mehr um die Versammlung gekümmert als um die Familie. Ständig mussten wir zurück stecken weil er in den Dienst müsste oder einen Vortrag halten.<br>
<br>
Eine weitere große Angst von mir ist immer dass irgendjemand erfahren könnte dass ich mal bei den Zeugen war. Aus meinem jetzigen Umfeld weiß das niemand und ich möchte nicht dass es jemand erfährt. Aber andererseits habe ich das Gefühl dass ob mit jemandem darüber sprechen muss, aber ich denke auch dass jemand der nie dabei war, mich einfach nicht verstehen kann. Deswegen bin ich jetzt hier und auch wenn meine Geschichte vielleicht nicht so schlimm ist wie andere hier, setzt es mir doch immer noch sehr zu.<br>
<br>
LG Dietlinde");

INSERT INTO `stories` (`img`, `date`, `header`, `name`, `text`) VALUES ("images/story-00000000002.jpg", "2018-05-11", "Acht Jahre durch die Hölle", "Christian Q.",
"Hallo an alle,<br>
Ich bin Christian, 21 Jahre alt und komme aus dem Grünen Herzen Deutschlands.<br>
In meinem Text zu meinem Lebensbericht habe ich schon genauer erwähnt, in was für einer Art von Sekte ich bis vor wenigen Jahren noch war und ich kann nur sagen, dass ich unglaublich froh bin ein eigenständiges Leben führen zu dürfen und mit der Zeit Menschen kennen gelernt zu haben, die mein Schicksal teilen und es ebenfalls geschafft haben, so unterschiedlich die jeweiligen Hintergründe auch sein mögen.<br>
<br>
Ich hoffe mich hier mit Menschen über das Thema Sekte auseinander setzen zu können, die selbst Betroffene waren oder sind, oder wissen, wie einem geholfen werden kann, denn tatsächliche und professionelle Hilfe habe ich bis heute nicht in Anspruch genommen.<br>
<br>
Ich wünsche allen noch einen guten Abend<br>
<br>
Schon vor ein paar Jahren habe ich mich damit auseinander gesetzt, wie mein Leben in der Gemeinschaft eigentlich so war.<br>
Chronologisch gesehen kann ich das recht fix zusammen fassen:<br>
In meinen ersten zwei Jahren, mit neun bis elf Jahren, war ich niemand. Ich war nicht wirklich Teil des Ganzen, auch, wenn ich mich, wie alle anderen, den geltenden Gesetzen zu unterwerfen hatte. Dabei wollte ich, wenn ich schon Mitglied sein musste, doch wenigstens beachtet werden, aber ich blieb auf der Strecke und unscheinbar, nicht interessant.<br>
Mit meiner Entjungferung, im Alter von elf, sollte ich erst integriert werden, als ein tatsächlich aktiver Teil. Da begann die Angst in mir zu siegen, was es nun heißen sollte, sich an der Gemeinschaft zu beteiligen, was ich dafür zu leisten hatte, wie ich mit meinen \"Brüdern und Schwestern\" umgehen sollte und wie sie mit mir umgehen würden etc ... Ab da begann mein Priester Interesse für mich zu entwickeln und das prägte mich schließlich für den Rest meines Lebens bis jetzt. Ich erhielt im Laufe der Zeit eine Sonderstellung, die mich sehr von den anderen unterschied. Ich war wieder nicht Teil des Ganzen, aber ich stand sogar über ihnen, da ich dem besonderen, nicht-bedingungslosen Schutz meines Priesters unterstand. In meiner radikaleren Zeit wandelte sich dieses Gefühl in starken Hochmut, was allerdings nicht von ungefähr kam, schließlich war ich auf dem Weg nach oben. Ich sollte selbst Priesterin werden, wozu es allerdings doch nicht kam, aber hätte kommen sollen. Ich lernte meine Macht zu erkennen, sie zu nutzen und zu lieben.<br>
Das hielt genau so lange an, bis ich beschloss auszusteigen. Ich war nie wirklich ein Teil der Gemeinschaft, existierte immer nur nebenher.<br>
Doch wenn ich so darüber nachdenke, glaube ich war das auch ganz gut so.");

INSERT INTO `stories` (`img`, `date`, `header`, `name`, `text`) VALUES ("images/story-00000000003.jpg", "1999-04-02", "Das Leben einer Zeugin", "Eva N.",
"Die an Waghalsigkeit grenzende Originalität der BeeGees weckt mich unsanft aus meinem Morgenschlummer. Tagwache für meinen Mann. Ich muss zwar noch nicht raus, an ein gemütliches, tiefes Weiterschlafen ist jedoch nicht zu denken, da meine hungrigen Katzen dies zu verhindern wüssten. Dennoch kann ich noch etwas länger liegenbleiben. Dies bedarf wohl einer Erklärung, da es doch Ehrensache für eine 'anständige' Ehefrau sein sollte, ihrem Mann das z'Morge zuzubereiten und mit ihm Morgengemeinschaft zu pflegen. Da wir Zeugen Jehovas grundsätzlich vor der Ehe nicht zusammenwohnen, können sich gewisse Alltagsabläufe erst nach der Eheschliessung einstellen. Wenn man nicht mehr 'blutjung' ist (wir sind beispielsweise beide um die 30 Jahre alt), mag dies manchmal Flexibilität und Anpassungsvermögen benötigen. Wir hatten damit jedoch keinerlei Probleme: nach zwei (von nunmehr insgesamt zehn) Ehemonaten, beschlossen wir, ich müsse nicht unbedingt gemeinsam mit Martin aufstehen, da wir ohnehin beide 'Zeit zum Aufwachen' brauchen (in meinem Fall könnte man dies als 'Morgenmuffelei' bezeichnen) und uns gerne in Ruhe auf den vor uns liegenden Tag konzentrieren. Dazu kommt, dass Martin als Hochbaupolier im Gegensatz zu mir als Büroangestellte sehr früh weg muss.<br>
<br>
Nachdem Martin also gegangen ist, tue ich dasselbe wie er: ich lese bei einer Tasse Kaffee den 'Tagestext' - eine Bibelstelle mit einem kurzen Kommentar, der zum Nachdenken anregt. In dieser ruhigen Morgenstunde, bevor die Hektik des Tages hereinbricht, spreche ich auch gerne ein Gebet und bereite mich auf den Tag vor.<br>
<br>
Nach 25-minütiger Stassenbahnfahrt durch halb Zürich komme ich etwa um 8.30 Uhr ins Büro, mache mir einen Ueberblick über die heutigen Pendenzen - und lege mit meiner Arbeit los. Um 10.00 Uhr brauche ich eine kurze Pause vom Computer und setze mich mit einer Arbeitskollegin in Richtung Cafeteria ab. Da ich vor einiger Zeit erwähnte, dass ich Zeugin Jehovas bin, möchte sie einige Fragen los werden. Ob es nicht Zeugen Jehovas seien, die von Tür zu Tür gehen, und ob ich dies auch täte, möchte sie wissen. Beides bestätige ich. Es folgt die unvermeidliche Frage: \"Muss das denn jeder bei euch machen?\" Viele Menschen haben diese Vorstellung, dass wir unter Zwang stehen (was nicht stimmt), oder dass wir eine Gehirnwäsche hinter uns haben (was absurd ist, wenn man bedenkt, woher der Ausdruck stammt und was er besagen sollte). Es ist für jemanden, der unseren Glauben nicht teilt vermutlich schwer nachvollziehbar, dass wir freiwillig und sogar gerne 'predigen'. Um die Frage zu beantworten, erkläre ich: \"Nein, es ist jedermanns eigene Entscheidung, ob er von Haus zu Haus geht, um mit den Menschen zu sprechen. Aber da es ein wichtiger Bestandteil unseres persönlichen, aktiven Glaubens ist, tun es die meisten.\" Wir unterhalten uns noch eine Weile über das Thema, doch schliesslich ist die Pause vorbei und wir müssen zurück an die Arbeit. Sicher wird sich wieder einmal die Gelegenheit für ein solches Gespräch ergeben.<br>
<br>
Am Mittag möchte mich ein Arbeitskollege zur Fitness-Stunde im Fitnesszentrum des Akademischen Sportvereins überreden, aber ich habe heute keine Zeit. \"Ach, Du gehst wieder reiten?\" mutmasst er, da er mich als Pferdeliebhaberin kennt und weiss, dass ich regelmässig reite. \"Nein, ich habe um 14.30 Uhr eine Verabredung.\" antworte ich. Er würde es wohl nicht ganz verstehen, wenn ich ihm erklärte, ich hätte mich 'für den Dienst' verabredet.<br>
<br>
Ich komme um ca. 13.15 Uhr nach Hause und gönne mir eine Weile Ruhe mit einem Sandwich und einem Buch. Ich liebe englische Literatur und verschlinge im Moment gerade 'The Return of the Native' von Thomas Hardy. Dabei kann ich mich herrlich entspannen.<br>
<br>
Danach bereite ich mich für den Dienst vor. Vor allem überprüfe ich, ob ich alles in der Tasche habe: meine Bibel, einige Zeitschriften und meine persönlichen Notizen, die mir aus verschiedenen Gründen sehr wichtig sind. Einerseits helfen sie mir, dort vorzusprechen, wo ich noch niemanden angetroffen habe, sowie interessierte Personen wieder aufzusuchen. In solchen Fällen möchte ich mich erinnern, welches Thema ich jeweils angesprochen oder welche Zeitschriften ich abgegeben habe. So kann ich an einem Punkt wieder ansetzen, der den Wohnungsinhaber interessiert. Andererseits möchte ich es vermeiden, bei jemandem nochmals zu läuten, der überhaupt kein Interesse hatte, denn dieser würde sich bestimmt belästigt fühlen.<br>
<br>
Um 14.30 Uhr treffe ich eine 'Schwester' in meinem Alter (wir nennen unsere Mitgläubigen \"Brüder\" und \"Schwestern\"), und wir ziehen los. Ich habe eine kleine Karte, auf welcher ein Gebiet von Häusern eingegrenzt ist. In diesem Gebiet versuche ich, nach und nach an jeder Wohnungstüre jemanden anzutreffen. An den ersten vier Türen öffnet niemand. Dann treffen wir eine junge Frau, die aufmerksam zuhört, während ich uns freundlich vorstelle und möglichst rasch zum Thema komme. Ich biete die Zeitschriften 'Der Wachtturm' und 'Erwachet!' an. 'Erwachet!' hat das passende Thema 'Gespräche über Religion'. Ich erwähne den Gedanken, dass respektvolle Gespräche uns helfen, unsere Standpunkte gegenseitig besser kennenzulernen. Dies baut Hemmungen und Vorurteile ab und fördert somit die Toleranz - etwas, das wir in der heutigen Zeit gut gebrauchen können. Die junge Frau stimmt mir da zu und nimmt die Zeitschriften entgegen.<br>
<br>
Wir treffen noch einige weitere Personen an, die uns bestimmt aber dennoch relativ höflich zu verstehen geben, sie hätten kein Interesse. Selbstverständlich respektieren wir das, verabschieden uns und wünschen jeweils einen schönen Tag. Ein Wohnungsinhaber ist weniger höflich: er lässt uns nicht ausreden sondern schnauzt uns nur an, wir sollten mit diesem 'Sektenmist' verschwinden und knallt uns die Türe vor der Nase zu. Wir sehen uns kurz betroffen an und meine Kollegin meint: \"Soviel zum Thema Anstand\". Es ist ärgerlich und verletzend, so behandelt zu werden, wenn man doch gar nichts Unrechtes getan hat. Zum Glück sind solche Vorfälle, wiewohl unangenehm, so doch sehr selten. Wir versuchen, es nicht persönlich zu nehmen, und da wir zu zweit sind, lassen wir uns auch nicht so schnell entmutigen. Erfreulicherweise können wir kurz darauf ein langes und interessantes Gespräch mit einer älteren Frau führen, die uns schliesslich sogar auf eine Tasse Tee einlädt. Bevor wir uns verabschieden, können wir einen weiteren Besuch mit ihr vereinbaren, was uns sehr freut.<br>
<br>
Nach insgesamt zwei Stunden beschliessen wir, uns auf den Heimweg zu machen. Alles in allem haben wir einen sehr schönen Nachmittag verbracht und sind so richtig positiv 'aufgeladen'.<br>
<br>
Ich muss auf dem Heimweg noch ein paar Sachen einkaufen und komme kurz nach 17.00 Uhr zuhause an. Etwa um 17.30 Uhr wird mein Angetrauter in der Türe stehen und 'Hunger' rufen. Um 18.00 Uhr essen wir zusammen, vorher spricht mein Mann ein kurzes Gebet, um Dank und Wertschätzung zum Ausdruck zu bringen. Wir nehmen uns gerne Zeit beim Essen und unterhalten uns über den Tag. Ich erzähle auch von den heutigen Erfahrungen im Dienst und stelle erneut fest, dass die Freude an guten Gesprächen schlechte Erfahrungen bei weitem aufwiegt.<br>
<br>
Da heute Dienstag - ein 'Zusammenkunfts-Tag' - ist, machen wir uns um etwa 19.15 Uhr wieder auf den Weg. Kurz vor 19.30 Uhr sind bereits fast alle in unserem schönen, schlichten 'Königreichssaal' versammelt, und ich fühle mich richtig wohl unter all meinen 'Brüdern und Schwestern'. Die Zusammenkunft beginnt mit Lied und Gebet, dann werden von mehreren getauften Zeugen kurze biblische Ansprachen gehalten. Auch ich habe heute einen Beitrag in Form eines Zwiegespräches vorbereitet, den ich schon gestern mit meiner Partnerin besprochen habe. Er dauert etwa fünf Minuten und handelt von einer biblischen Person des Alten Testaments und ihrer Vorbildwirkung für uns. Kurz nach 21.00 Uhr wird mit Lied und Gebet abgeschlossen. Ich habe heute Abend einige sehr gute biblische Gedanken notiert, denen ich nächstens zuhause mit meiner Bibel nochmals nachgehen möchte.<br>
<br>
Nach dem Programm unterhalten Martin und ich uns jeweils noch gerne mit unseren Freunden, treffen Abmachungen für den Dienst, vereinbaren gemeinsam Einladungen zum Essen oder Verabredungen fürs Kino oder andere Tätigkeiten. Wir geniessen die entspannte und freundliche Atmosphäre sehr und pflegen gerne die Gemeinschaft mit unseren Mitgläubigen. Auch heute wird es wieder fast 22.00 Uhr bis wir nach Hause kommen.<br>
<br>
Später im Bett unterhalten Martin und ich uns noch über einige Gedanken aus der Zusammenkunft, aber auch was wir in den nächsten Tagen vorhaben. Am Wochenende soll das Wetter schön sein, in dem Fall können wir endlich eine Fahrradtour machen. Wir freuen uns beide riesig auf den Frühling, wenn die Natur wieder erwacht und aufblüht und die Vögel mit ihrem herrlichen Gesang zurückkehren! Bei einer Fahrradtour oder einer Wanderung durch den Wald kann man die wunderbare Schöpfung in vollen Zügen geniessen. Sonntag Nachmittag könnten wir bei Martins Eltern reinschauen, ich könnte einen Apfelstrudel machen, das geht schnell und ist fein.<br>
<br>
Martin und ich geniessen diese Zeit gemeinsam sehr. Wir sind noch nicht lange verheiratet und freuen uns, zu zweit zu sein. Zurzeit haben wir noch keine Pläne für Kinder, aber sowas geht ja manchmal schneller, als man meint. Auf jeden Fall ist es uns jetzt wichtig, unsere Beziehung zu vertiefen und zu pflegen, damit sie in Zukunft auch mal eine Hürde überwinden oder einen Sturm überstehen kann. Wir können immer noch viel übereinander erfahren und voneinander lernen. Zum Beispiel ist Martin in einer Familie von Zeugen Jehovas aufgewachsen, ich hingegen wurde römisch-katholisch erzogen und lernte die Bibel erst viel später, vor etwa sechs Jahren, kennen und schätzen. Diese Unterschiede in unseren Lebenserfahrungen sind interessant für uns beide, können aber durchaus auch für Missverständnisse oder Meinungsverschiedenheiten sorgen. Auf jeden Fall ist es für uns beide schön und beruhigend zu wissen, dass wir in grundsätzlichen Fragen des Lebens und der Anbetung übereinstimmen. Auch dies wird in unserer Ehe immer für eine enge Bindung zwischen uns sorgen.<br>
<br>
So, nun wird es Zeit zu schlafen. Bald ist es wieder Mitternacht, und morgen ist ein Arbeitstag. Martin ist bereits so müde, dass er schon während des Gesprächs fast eingeschlafen ist. Na, dann beten wir halt morgen wieder gemeinsam. Morgen Nachmittag habe ich mir nichts Festes vorgenommen - ab und zu mal Ausspannen muss sein, und der Haushalt will auch noch gemacht werden. Heute bin ich müde aber glücklich und habe das Gefühl, einen sehr schönen und sinnvollen Tag verbracht zu haben. All dies bringe ich noch in einem Gebet zum Ausdruck bevor ich endgültig und ohne die sonst gewohnte Nachtlektüre einschlafe. Ich denke noch, hoffentlich habe ich morgen mehr Glück und darf ohne die BeeGees in den Tag starten.");

INSERT INTO `stories` (`img`, `date`, `header`, `name`, `text`) VALUES ("images/story-00000000004.jpg", "2010-10-09", "Freiheit nach 21 Jahren Gefangenschaft unter den ZJ", "Tom S.",
"Ich grüße alle Leser von SADS<br>
<br>
Vor 4 Jahren trat ich aus der Gemeinde der Zeugen Jehovas aus und traue mich erst heute (!) darüber offen zu reden, bzw. diese Leidensgeschichte zu erzählen, doch zunächst ganz von vorne:<br>
<br>
Ich bin seit meiner Geburt in der Sekte/dem Verein/ der Religion aufgewachsen, und als Kind ist man der Gehirnwäsche Tag für Tag gnadenlos ausgesetzt.<br>
\"Moment mal, die 'Zusammenkünfte' sind aber nicht jeden Tag!\" mag sich wahrscheinlich ein Aussteiger der Sekte womöglich denken, und jene (oder Du) haben (hast) auch recht. Die Zusammenkünfte wurden 2 mal die Woche besucht. Woche für Woche, Monat für Monat und Jahr für Jahr<br>
<br>
Ich erinnere mich noch haargenau daran, dass meine 2 kleineren Brüder und Ich als Kleinkind(!) uns jeden Abend Horrorgeschichten aus einem großen gelben Geschichtenbuch (herausgegeben von der hiesigen Organisation) vorgelesen wurde. Jeden Abend wurde uns vermittelt: Gott wird alle töten, die ihm nicht gehorchen; Ein \"Gott der Liebe\" wird alle Ungläubigen hinrichten und die Erde in Blut tränken. (Der Leser lasse sich bitte die Ironie auf der Zunge zergehen); Gott sieht alles immer und zu jeder Zeit; Jeder wird für seine Taten gerichtet ...<br>
<br>
Als ich lesen lernte, kamen meine Ersten Sätze aus der Bibel<br>
Mir wurde schon vor der Einschulung beigebracht, andere Kinder zu meiden, die nicht den gleichen Glauben teilen.<br>
<br>
Für mich war es auch selbstverständlich keine Geburtstage zu feiern, kein Weihnachten, Ostern o.ä.<br>
Ich empfand diese Feste sogar als abgrundtief böse<br>
<br>
Ich war also von klein auf fundamentalisiert.<br>
<br>
<br>
Im Alter von 14 Jahren trennten sich meine Eltern, da mein Vater zum Alkoholiker wurde, meine mutter mehrmals betrog, und er dementsprechende Konsequenzen zu spüren bekam (ausschluss aus dem Verein), ein halbes Jahr darauf verstarb mein Vater. Er hatte sich quasi zu Tode gesoffen.<br>
Wir lebten allesamt seit der Trennung bei meiner Mutter und meiner Großmutter, die als ein echtes Urgestein bei den Zeugen Jehovas gilt. Als die Nachricht aus dem Krankenhaus kam, dass mein Vater zusammengebrochen ist und im Koma lag, wurde es kaum thematisiert - klar wir waren besorgt und traurig. Aber er war halt ein Abtrünniger.<br>
Er verstarb wenige Wochen nach der Einlieferung. Bei seiner Beerdigung auf die wir gingen sagte doch ein ehemaliger Glaubensbruder \"Er hat für seine Sünden bezahlt, wir werden uns im Paradies wiedersehen\" -Moment mal-:<br>
<br>
Wenn tatsächlich gesagt wird, dass man mit seinem Tod seine Sünden bereinigt, ist dies dann nicht ein indirekter Aufruf zum Suizid? - Dieser Gedanke hatte sich seit der Beerdigung in meinem Kopf festgebrannt und war die Geburtsstunde meiner Zweifel. Wohlgemerkt: mit 14 Jahren.<br>
<br>
<br>
Ab dem 15. Lebensjahr fingen die Mädchen an, interessant zu werden. Meine Pubertät schlug mit voller Härte (Doppeldeutig denken ist hier gerade erwünscht) zu. Mit anderen Worten: Der Sexualtrieb der durch eine erhöhte Testosteronbildung zurückzuführen ist -welches normal ist - begann, und damit auch eine sehr schwierige Phase als junger Zeuge.<br>
<br>
Als Zeuge Jehovas sind praktiken wie die Mastrubation \"unrein\" und werden regelrecht verteufelt. Man neigt dazu dies heimlich zu zu praktizieren. Durch die Erziehung weiß man jedoch, dass Gott alles sieht. So wurde mein Gewissen jedes mal geplagt. Auch wenn ich anfing mich für ein Mädchen aus der Schule zun interessieren und nur darüber nachzudenken, hatte ich schon Gewissenbisse.<br>
<br>
Ich fing an mich in der Schule zu isolieren, und wurde dementsprechend gemobbt. Mein \"Anderssein\" fiel in der Schule auf, und so blieben die Mobbingattacken nicht nur in der Klasse sondern auch Klassenübergreifend.<br>
Ich dachte mir \"das ist ok. ich sündige schwer, das ist meine strafe\". Die Erlebnisse in der Schule sind selbst heute mit 25 Jahren noch nicht ganz verarbeitet.<br>
<br>
Zum Glück konnte ich damals in der Versammlung Freunde finden, mit denen ich nach der schule häufiger was unternahm.<br>
Mit 18 Jahren lernte ich meine erste Freundin kennen. Sie ist bis heute noch eine getaufte Zeugin Jehovas. Wir lernten uns in der Versammlung kennen und waren direkt schwer ineinander verliebt. Wir hatten uns sogar nach einem Jahr verlobt (mit 19 Jahren), und hatten -verbotenerweise- häufiger Sex vor der Ehe, was wir strikt geheim hielten.<br>
<br>
Als Zeuge Jehovas gilt es als schwere Sünde Geschlechtsverkehr vor der Ehe zu haben. Dies hat i.d.R. einen Ausschluss, oder Zurechtweisung zur Folge<br>
<br>
In der Zwischenzeit lernte meine Mutter einen neuen Mann kennen, der noch nicht getauft war aber sehr wohl die Bibel studierte. Sie waren eine kurze Zeit lang ein inoffizielles Pärchen, und er ließ sich (m.E. nach wegen ihr) als Zeuge Jehovas taufen. wenige Wochen darauf heirateten sie, und wir zogen von meiner Großmutter weg in eine eigene Mietwohnung.<br>
Es ging sage und schreibe 3 Monate gut. Bis er anfing nur mich fast jede Woche mehrmals zu schlagen, zu mobben oder zu drangsalieren. Ich erfragte Hilfe von den Ältesten und meldete die Vorfälle. Doch sie machten rein gar nichts. Sie führten nich einmal ein klärendes Gespräch mit ihm.<br>
<br>
Und wieder dachte ich, dass es eine Bestrafung Gottes sei, die ich durchzustehen habe. Mit jeder Woche hielt ich seine Schlägen stand.<br>
ich aß nicht mehr mit meiner Familie an einem Tisch, da er mir das untersagte.<br>
Ich wurde quasi psychisch und physisch von ihm jeden Tag über 1 Jahr lang hinweg von ihm regelrecht geprügelt oder beschimpft/verspottet, und ich redete mir jedesmal ein, dass Jehova mich für meine Taten bestrafen will.<br>
<br>
Der Gedanke der Beerdigung meines Vaters kam wieder in mir hoch, und ich unternahm einen Suizidversuch, den ich -offensichtlich- überlebte. Erst danach unternahm meine Mutter was. Sie schmiss .... mich raus! Ja, richtig gelesen. Sie schmiss mich raus, weil sie Angst um mein Leben hatte.<br>
<br>
Also zog ich wider Willen in meine erste eigene Wohnung. Unterstützung bekam ich seitens der Ältesten und Brüder nicht.<br>
Meine Freundin war damals alles was ich zu dem Zeitpunkt hatte, und gab<br>
mir Trost und die Kraft das alles ein wenig zu verarbeiten.<br>
<br>
Eines Tages, als mein damaliger bester Freund und Ich bei meiner Freundin übernachteten musste ich eines frühen Morgens aufstehen, da mich ein enormer Durst überkam.<br>
Zum Verständniss:<br>
Ich und mein damaliger bester schliefen in ihrem Schlafzimmer auf einem Doppelbett. Meine Freundin auf dem Schlafsofa im Wohnzimmer.<br>
Um in die Küche zu gelangen muss man das Wohnzimmer passieren.<br>
<br>
Ich stand eines morgens auf und wollte zur Küche, und vernahm schon im Flur Geräusche. Als ich ins Wohnzimmer betrat erwischte ich die beiden beim Sex.<br>
Ich bekam einen schweren nervenzusammenbruch und kam ins krankenhaus um mich zu erholen. Die Ältesten besuchten mich und machten sich Sorgen.<br>
Ich wollte es ihnen erzählen, brachte aber kein wort heraus. der schock saß noch immer zu tief. Wir vereinbarten zu einem besseren Zeitpunkt darüber zu sprechen.<br>
<br>
Als ich die station nach 2 Wochen verließ, fuhr ich ins Haus meiner (noch) Freundin. Ich wollte mir ihr ein klärendes Gespräch führen, flüchtete aber. Sie konnte nicht mit mir in einem Raum bleiben.<br>
Also ging ich in ihr schlafzimmer und entdeckte ihren Laptop. Eingeschaltet. Facebook offen.<br>
<br>
ich nahm mir die Freiheit den Chatverlauf der beiden auszukundschaften und kopierte den GESAMTEN Chatverlauf und speicherte ihn als extra Dokument ab. Aus dem Chatverlauf ergab sich, dass deren Techtelmechtel nicht das einzige war, sondern der Sche* 3 Monate ging.<br>
Reden konnte ich nicht mit ihr darüber, da sie nur auswich, und vor mir flüchtete. Also fuhr ich nach hause, und bat um einen Ältesen um mit der Sache zu reden.<br>
<br>
Der Älteste am Telefon und Ich machten für den Folgetag um 17 Uhr einen Termin aus. Eine halbe Stunde vor dem Termin, wurde ich versetzt. warum wollte er mir nicht sagen.<br>
<br>
wie sich hinterher herausstellte, hatte meine ex Freundin den gleichen Ältesten vorher \"beordert\" und erzählte davon, das wir Sex gehabt hatten.<br>
Sie verriet mich, und zeitgleich hielt sie die Affaire mit meinem ex besten Kumpel geheim.<br>
Ich kam dennoch dazu dem Ältesten meine Version zu berichten und zeigte ihm sogar meine kopierten Chatverläufe zwischen den beiden.<br>
<br>
Da sie jedoch getauft war, wurde ihr mehr glauben geschenkt als einem \"nicht getauften\" die Ältesten unternahmen nicht einmal die Anstrengung dem nachzugehen. Ich verlor mein Amt als ungetaufter Verkündiger und wurde in der Versammlung als dies verkündet wurde öffentlich bloßgestellt.<br>
Sie kam mit einer müden zurechtweißung. Kein Ausschluss. Kein bloßstellen. nur ein müdes Zeigefingerwedeln.<br>
<br>
Ich fing an die Zusammenkünfte seltener zu besuchen, und bekam promt Besuche von \"besorgten\" Ältesten. Diese Besuche waren jedoch eher ein \"Wieso besuchst du uns nicht mehr\" als ein \"Geht´s dir gut?\" ... Meine Skepsis zu der Sekte wuchs damit immer mehr, bis ich schließlich mit 21 Jahren den Verein verließ.<br>
<br>
Meine Mutter und meine Brüder brachen den Kontakt ab, als sei ich<br>
ein Geschwür in der Familie.<br>
<br>
Unter den Folgen litt ich so sehr, dass ich mich vorletztes Jahr freiwillig<br>
in die Psychiatrie einweißen ließ, da die selbstzerstörerischen Gedanken überhand nahmen.<br>
<br>
Bis heute spüre ich noch die Auswirkungen der Lehren der zeugen Jehovas.<br>
Selbst nach 4 Jahren, habe ich bisher nur 1 mal meinen Geburtstag und Weihnachten gefeiert. Ich komme bis heute nicht aus diesem \"Drill\" heraus.<br>
Zum Glück konnte ich es ablegen, mir wegen allem Gewissensbisse zu bekommen, oder gar selbst zu schüren.<br>
<br>
Im nachhinein war es die Beste Entscheidung die ich machen konnte, dem Verein den Rücken zu kehren. Die Freiheit musste ich jedoch mit einer dauerhaft geschädigten Psyche und dem Verlust meiner Familie schwer erkaufen. Und dennoch war es die Richtige Entscheidung<br>
<br>
Zeugen Jehovas? Nein danke");

INSERT INTO `stories` (`img`, `date`, `header`, `name`, `text`) VALUES ("images/story-00000000005.jpg", "2016-09-01", "Hilfe beim Austieg der Freundin", "Peter Y.",
"Hallo Zusammen,<br>
<br>
ich habe gestern geschrieben, dass meine Freundin / Verlobte (3 KInder; ungetaufte Zeugin seit 10 Jahren; kam hin nach häuslicher Gewalt durch den Ex. Arbeitskollegin sprach Sie dann an) und mit der ich seit 2 Jahren inkl. regelmäßigem Sex zusammenwohne, sich wieder intensiv den Zeugen zugewandt hat. Dies nachdem Sie innerhalb eines Jahre nur 4x dort war (ohne auch nur einen Kongress zu besuchen!). Ich hatte nicht gegen die ZJ gearbeitet, sondern ihr Selbstwertgefühl gesteigert und ihr mehr Freiheit gegeben. Ich bin dann zum Nachdenken ausgezogen, damit wir über uns nachdenken können. Der Rückfall kam durch zwei Problemfälle (älteste Tochter ging mit 15 Jahren wieder zum Vater, der alle schlug und Sie muss aufstocken beim Amt). Nach dem 3 Tageskongress vor 2 Wochen war Sie dann komplett wieder \"auf Droge\" (will wieder in die Wahrheit => kein Sex, kein Zusammenwohnen vor der Ehe, viel von Hauis zu Haus gehen, will immer in jede Versammlung, jeden Kongress. Auch die Kleine mit 7 Jahren muss seit 4 Wochen Bibelstudium machen). Beweise in der Bibel findet Sie für die Behauptungen kein Sex vor der Ehe natürlich nicht. Sie betreibt auch keine Hurerei, wie ihr eingeedt wird. Natürlich ist Sie Artikel und Infos aus dem Internet nicht zugänglich (Satanszeug). Findet aber keine Beweise in der Bibel (auch nicht über ihre \"Bibelstudiumtanten\"). Aussagen: Das weis doch jedes Kind lies ich nicht gelten.<br>
<br>
Hat jemand gute Ideen, ob es noch eine Chance gibt und was ich tun könnte?<br>
<br>
Meine Überlegungen sind folgende:<br>
Ich gehe mit einem Brief über die Mutter (Inhalt: was machen die Zeugen, weshalb liebe ich die Tochter, wie hat diese sich verändert, ich mag ihre drei Kinder und will nicht, das diese auch \"versaut werden fürs Leben\"), die keine Zeugin ist. Wie kann diese überzeugen?<br>
Ich schreibe ihr einen emotionalen, aber ergebnisoffener Brief. Inhalt: Liebe dich, beide brauchen mehr Freiräume, haben beide Fehler gemacht, die positiven Dinge überwiegen, ich mag an Dir: Nähe, Aussehen, lachen, reden können,.... Lass uns in 6 Wochen (nach ihrer Mutter-Kind Kur) zusammensetzen und die Zukunft besprechen<br>
Ich gehe über Ihre Cousine (keine Zeugin), mit der Sie früher oft zusammen war und die mit ihr reden will (Sie macht es. Wie berzeugt Sie?)<br>
Die Älteste Tochter, die die eifrigste Zeugin war (ungetaufte Verkünderin), geht dort nicht mehr jin (wollte dies seit 2 Jahren nicht, da Sie Zweifel hatte ide ich bestärkte). Blieb aber wegen der Mutter dort. Erwähnte bei der Mutter nun, dass dies aus eigener Überzeugung geschah, und nicht weil die Freundin nicht mehr in die Versammlung ging (das denkt diese !). Bei der Mutter-Kind Kür würde die Älteste ggf. die Mittlere Tochter / Mutter auf das thema ansprechen. Wie sollte das geschehen?<br>
<br>
Vielen Dank schon im Voraus für eure Anregungen<br>
Liebe Grüße<br>
<br>
Peter");
