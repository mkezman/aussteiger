﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aussteiger.Components.Persistence.ORM;
using aussteiger.DataObject.REST;
using aussteiger.Utils;
using DbLinq.Data.Linq;

namespace aussteiger.Components.Persistence
{
    public class PersistenceManager
    {
        private readonly object LockObject = new object();

        private string TrimRqBody(string rqBody)
        {
            return rqBody.Trim('\"');
        }

        #region stories

        public List<Stories> GetThreeStories()
        {
            lock (LockObject)
            {
                using (var ctx = MySqlHelper.GetDataContext())
                {
                    var rnd = new Random();
                    var stories = ctx.Stories.ToList();
                    foreach (var story in stories)
                    {
                        story.Text = "";
                    }
                    return stories.ToList().OrderBy(x => rnd.Next()).Take(3).ToList();
                }
            }
        }
        public List<Stories> GetThreeStories(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                id = "1";
            }
            var intId = int.Parse(id);
            lock (LockObject)
            {
                using (var ctx = MySqlHelper.GetDataContext())
                {
                    var rnd = new Random();
                    var stories = ctx.Stories.Where(s => s.ID != intId).ToList();
                    foreach (var story in stories)
                    {
                        story.Text = "";
                    }
                    return stories.OrderBy(x => rnd.Next()).Take(3).ToList();
                }
            }
        }

        public Stories GetStoryById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                id = "1";
            }
            var intId = int.Parse(id);
            lock (LockObject)
            {
                using (var ctx = MySqlHelper.GetDataContext())
                {
                    return ctx.Stories.First(s => s.ID == intId);
                }
            }
        }

        #endregion

        #region country

        public List<EInWoHNerdICHTe> GetCountries(string rqBody)
        {

            var query = SerializationUtility.DeserializeObject<CountryQuery>(rqBody);
            lock (LockObject)
            {
                using (var ctx = MySqlHelper.GetDataContext())
                {
                    var rnd = new Random();
                    var countries = ctx.EInWoHNerdICHTe.Where(e => e.EInWoHNeRKM >= query.PopulationMin && e.EInWoHNeRKM <= query.PopulationMax && e.KoNtInEnT == query.Continent).ToList();
                    return countries.ToList().OrderBy(x => rnd.Next()).Take(3).ToList();
                }
            }
        }

        #endregion
    }
}
