﻿using System.Net;
using System.Text;

namespace aussteiger.Components.Communication
{
    public class ResponseWriter
    {
        public static void WriteText(HttpListenerResponse response, string message)
        {
            if (message == null)
            {
                response.StatusCode = 400;
                return;
            }

            response.ContentType = "text/plain";
            var buffer = Encoding.UTF8.GetBytes(message);
            response.ContentLength64 = buffer.Length;
            using (var outputStream = response.OutputStream)
            {
                outputStream.Write(buffer, 0, buffer.Length);
            }
        }

        public static void WritePage(HttpListenerResponse response, string url, byte[] pageContent)
        {
            if (pageContent == null)
            {
                response.StatusCode = 400;
                return;
            }

            response.ContentType = GerContentType(url);
            response.ContentLength64 = pageContent.Length;
            using (var outputStream = response.OutputStream)
            {
                outputStream.Write(pageContent, 0, pageContent.Length);
            }
        }

        private static string GerContentType(string url)
        {
            if (url.EndsWith(".css"))
            {
                return "text/css";
            }

            if (url.EndsWith(".html"))
            {
                return "text/html";
            }

            if (url.EndsWith(".js"))
            {
                return "application/javascript";
            }

            if (url.EndsWith(".png"))
            {
                return "image/png";
            }

            if (url.EndsWith(".otf"))
            {
                return "application/x-font-otf";
            }

            if (url.EndsWith(".eot"))
            {
                return "application/vnd.ms-fontobject";
            }

            if (url.EndsWith(".svg"))
            {
                return "image/svg+xml";
            }

            if (url.EndsWith(".ttf"))
            {
                return "application/x-font-ttf";
            }

            if (url.EndsWith(".woff"))
            {
                return "application/x-font-woff";
            }

            if (url.EndsWith(".woff2"))
            {
                return "font/woff2";
            }

            return "text/plain";
        }
    }
}
