﻿function InitVue() {
    var app = new Vue({
        el: '#MainContent',
        data: dropoutViewState,
        methods: {
            vueShowResult: function (data) { showResult(data); },
            vueGetResult: function() { getCountries(); }
        }
    });
}